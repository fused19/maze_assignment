/*
Student id: 20111375
module: Advanced programing
assignment: 1
date created: 01/01/2013
date last modified: 21/01/2013
version: 1 of 1
maze.cpp
*/

//maze.cpp is the implimentation file of maze.h

#include <iostream>
#include <iomanip>
#include <stdlib.h>
#include <time.h>
#include <vector>
#include <math.h>
#include "CO2451maze20111375.h"
using namespace std;
//////////////////////////////////////////////////////////////////////////////
maze::maze()// empty constructor
{
    playerScore = 0;// initialise the score to zero;
    srand(time(0));// this is a one time call to setup s pseudo random number generator
}
//////////////////////////////////////////////////////////////////////////////
maze::~maze()
{
// empty destructor
// 'new' isn't call so delete isn't needed
}
//////////////////////////////////////////////////////////////////////////////
void maze::matrices(int& HEIGHT, int& WIDTH)// setup a maze
{
    int randIndex, randPoint;
    bool found = false;
    vector<int> markDoorways(4);
    vector<int> playerLocation(4);
    vector<int> rows(WIDTH);
    vector<vector<int> > holdMaze(HEIGHT,rows);
    mazeperformTraversal = false;
//////////////////////////////////////////////////////////////////////////////
    for(int row = 0; row < HEIGHT; row++)//nested loop to construct maze
    {
        for(int column = 0; column < WIDTH; column++)
        {
            if(column == 0 || column == WIDTH-1)//make a perimeter of unbroken walls around the maze
            {
                holdMaze[row][column] = 1;
            }
            else if(row == 0 || row == HEIGHT-1)//make a perimeter of unbroken walls around the maze
            {
                holdMaze[row][column] = 1;
            }

            else
            {
                randIndex = rand() % 100 + 1;// randomly pick whether this index is a wall or a path
                if(randIndex <= 60)// 60% of it being a path and 40% of it being a wall
                {
                    holdMaze[row][column] = 0;// 0 is the code for path
                }
                else
                {
                    holdMaze[row][column] = 1;// 1 is the code for wall
                }
            }
        }
    }
    do
    {
        randPoint = rand() % (HEIGHT-2) + 1;
        if(holdMaze[randPoint][1]== 0)
        {
            holdMaze[randPoint][0] = 5;// 5 is the code for entrance
            markDoorways[0] = randPoint;
            markDoorways[1] = 1;
            found = true;
        }
    }
    while (!found);
    found = false;
    do
    {
        randPoint = rand() % (HEIGHT-2) + 1;
        if(holdMaze[randPoint][WIDTH-2]== 0)
        {
            holdMaze[randPoint][WIDTH-1] = 4;// 4 is the code for exit
            markDoorways[2] = randPoint;
            markDoorways[3] = WIDTH-1;
            found = true;
        }
    }
    while (!found);
    found = false;
    mazeMatrix = holdMaze;
    entrances = markDoorways;
    bool test = traverse(entrances[0], entrances[1]);// pass the enter coordinates
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
This section cleans up after the tranversal routine has run successfully and reset the path to 0's. Also, due to the
nature of the maze (which must have a solide perimitter for the traversal routine to work the enter and exit points have to be
passed from enterances to playerLocation .
*/
    if(!test)
    {
        maze::matrices(HEIGHT, WIDTH);//recursively call the matrices method until a traversable maze can be produced.
    }
    else// the traverse method has found a maze which can be traversed from enter to exit
    {
        for(int i=0; i < mazeMatrix.size(); i++)
        {
            for(int j=0; j<mazeMatrix[i].size(); j++)
            {
                if (mazeMatrix[i][j] == 3 || mazeMatrix[i][j] == 4)//reset traversed path and exit
                {
                    mazeMatrix[i][j] = 0;// replace traverse path with maze path
                }
            }
        }
    }
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
this is a fix for a problem encountered when trying to move
the player only one step at a time.
*/
    playerLocation[0] = entrances[0];
    playerLocation[1] = 0;
    playerLocation[2] = entrances[2];
    playerLocation[3] = entrances[3];
    playerPos = playerLocation;
}

bool maze::traverse(int row1, int column1)
{
/*
To work correctly the maze must be totally enclosed (no enter and exit point). A simple recursion
algorithm is applied to see if the exit can be reached. This is achieved by looking for the next
free space until no other free space can be found or the exit has been found. If no free space is
found then backtrack to the beginning, marking the path as visitied and start again.
*/
    if (row1 == entrances[2] && column1 == entrances[3])// step 1. compare the row1 and column 1 to the exit coordinates
    {
        mazeperformTraversal = true;
    }
    if(mazeMatrix[row1][column1] == 0)// step 2. do this while current coordinates are open
    {
        mazeMatrix[row1][column1] = 3;// step 3. mark a traversed path
        traverse(row1, column1+1);// step 4. continue
        traverse(row1-1, column1);
        traverse(row1+1, column1);
        traverse(row1, column1-1);
    }
    return mazeperformTraversal;
}
//////////////////////////////////////////////////////////////////////////////
void maze::printMaze()
{
    int count, innerCount;
    for(count=0; count < mazeMatrix.size(); count++)
    {
        for(innerCount=0; innerCount<mazeMatrix[count].size(); innerCount++)
        {
            if(mazeMatrix[count][innerCount]==1)
            {
                cout << '#';// convert 1's to hashes for walls
            }
            else if (mazeMatrix[count][innerCount]== 5)
            {
                cout << 'X';// convert 5's to player position X
            }
            else
            {
                cout << '.';// convert all other integers to '.' or paths
            }
        }
        cout << endl;
    }
}
//////////////////////////////////////////////////////////////////////////////
void maze::printMaze(bool& obfuscate)
{
    int count, innerCount;
    for(count=0; count < mazeMatrix.size(); count++)
    {
        for(innerCount=0; innerCount<mazeMatrix[count].size(); innerCount++)
        {
            if(mazeMatrix[count][innerCount]==1 || mazeMatrix[count][innerCount]==0)
            {
                cout << '*';//path
            }
            else if (mazeMatrix[count][innerCount]== 5)
            {
                cout << 'X';//player
            }
            else
            {
                cout << '.';//path
            }
        }
        cout << endl;
    }
}
//////////////////////////////////////////////////////////////////////////////
bool maze:: player(int& direction)
{
    bool winner = false;// does the players position equal the exit coordinates
    int xPos = playerPos[0], yPos = playerPos[1];// start the player at the enterance coordinates
    if(direction == 1)//north
    {
        if(mazeMatrix[xPos-1][yPos] == 0)
        {
            mazeMatrix[xPos][yPos]= 0;
            xPos -= 1;
            playerPos[0] = xPos;
            mazeMatrix[xPos][yPos]= 5;//change the int at maze location to 5 or player 'X'
            playerScore += 1;//increase score be 1
        }
        else
        {
            cout << endl;
            cout << "Illegal move" << endl;
            playerScore--;
        }
    }
    if(direction == 2)//east
    {

        if(mazeMatrix[xPos][yPos+1] == 0)
        {
            mazeMatrix[xPos][yPos]= 0;
            yPos += 1;
            playerPos[1] = yPos;
            mazeMatrix[xPos][yPos]= 5;
            playerScore += 1;
        }
        else
        {
            cout << endl;
            cout << "Illegal move" << endl;
            playerScore--;
        }
    }
    if(direction == 3)//south
    {
        if(mazeMatrix[xPos+1][yPos] == 0)
        {
            mazeMatrix[xPos][yPos]= 0;
            xPos +=1;
            playerPos[0] = xPos;
            mazeMatrix[xPos][yPos]= 5;
            playerScore+= 1;
        }
        else
        {
            cout << endl;
            cout << "Illegal move" << endl;
            playerScore--;
        }
    }
    if(direction == 4)//west
    {
        if(mazeMatrix[xPos][yPos-1] == 0)
        {
            mazeMatrix[xPos][yPos]= 0;
            yPos -=1;
            playerPos[1] = yPos;
            mazeMatrix[xPos][yPos]= 5;
            playerScore += 1;
        }
        else
        {
            cout << endl;
            cout << setw(50) << "Illegal move. Try agian!" << endl;
            playerScore--;
        }
    }
    if (playerPos[0] == playerPos[2] && playerPos[1] == playerPos[3])//compare player to exit coordinates.
    {
        cout << endl;
        cout << "HOORAY! You've won!" << endl;
        winner = true;
    }
    cout << " "<< endl;
    cout << "player X position: " << xPos << " " << "player Y position: " << yPos << endl;
    cout << " "<< endl;
    return winner;
}
//////////////////////////////////////////////////////////////////////////////
void maze::score()
{
    cout << " "<< endl;
    cout << "Your score is: " << playerScore << endl;
}
