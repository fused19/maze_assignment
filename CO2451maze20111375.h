/*
Student id: 20111375
module: Advanced programing
assignment: 1
date created: 01/01/2013
date last modified: 21/01/2013
version: 1 of 1
maze.h
*/

// maze.h is the class prototype

#ifndef MAZE_H
#define MAZE_H

using namespace std;

class maze
{
public:
    maze();
    ~maze();
    void matrices(int&, int&);
    void printMaze();
    void printMaze(bool&);
    bool player(int&);
    void score();

private:
    bool traverse(int , int);//check maze can be traversed
    vector<vector<int> > mazeMatrix;//store maze
    vector<int> entrances;//store coordinates for entrance and exit
    vector<int> playerPos;// store player coordinates
    bool mazeperformTraversal;// maze traversal bool
    int playerScore;// store player score
};

#endif // MAZE_H
