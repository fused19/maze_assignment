/*
Student id: 20111375
module: Advanced programing
assignment: 1
date created: 01/01/2013
date last modified: 21/01/2013
version: 1 of 1
main.cpp
*/

//main.cpp is the game and uses classes implimented in maze.cpp

#include <ctype.h>
#include "CO2451maze20111375.cpp"

int main()
{
    char yes;
    int width, height, direction;
    char response[]=" ";
    bool mazeperformTraversal = false;
    bool winner = false;
    bool hide = false;
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    maze game;// instantiate an instance of the maze class
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    do // main game loop hat initialises, displays and updates
    {
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        do // initialise the maze
        {
            cout << setw(68) << "Enter difficulty 0 = EASY AS PIE and > 0 = HARD AS HELL : ";
            cin >> response;
            if (isdigit(response[0])) // sanities the input and extract any integers
            {
                hide = atoi (response); // pass response to hide as an integer
            }else{
            cout << "Try again " << endl;
            }
        }
        while(!isdigit(response[0]));
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        do // set width of maze
        {
            cout << setw(41)<< "Enter width between 10 and 25: ";
            cin >> response;
            if (isdigit(response[0])) // as above
            {
                width = atoi (response);
            }
        }
        while(width < 10 || width > 25);
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        do // set height of maze
        {
            cout << setw(42)<< "Enter height between 10 and 25: ";
            cin >> response;
            if (isdigit(response[0])) // one last time
            {
                height = atoi (response);
            }
        }
        while(height < 10 || height > 25);
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        system("cls"); // clear the screen
        game.matrices(height, width); // define the maze size
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        if(hide) // pick which print maze method is invoked from the maze class
        {
            game.printMaze(hide);// print the hashed version for hard difficulty level
        }
        else
        {
            game.printMaze();// print the unhashed verion for the easy difficulty level
        }
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        do // update and display
        {
            cout << " "<< endl;
            cout << "*** Your current position is marked with an 'X' ***" << endl;
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            do // ask the player which direction they want to go in
            {
                cout << "Make a move. 1=North, 2=East, 3=South, 4=West :";
                cin >> response;
                if (isdigit(response[0]))
                {
                    direction = atoi (response);
                }
            }
            while(direction < 1 || direction > 4);
            system("cls"); // clear the screen
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            winner = game.player(direction);// get the boolean from player and see if the game is won yet
            if(hide) // pick which print maze method is invoked from the maze class
            {
                game.printMaze(hide);
            }
            else
            {
                game.printMaze();
            }
            game.score(); // print the players score
        }
        while(!winner);// do this until the win boolean is returned from maze class method player()
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        cout << " "<< endl;
        cout << "Do you want to quit? Please y for yes or any other key for no: ";
        cin >> yes;
        cout << " "<< endl;
    }
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    while (yes != 'y');// check if the player wants to play again
    cout << "Goodbye!" << endl;
    return 0 ;
}
